
module.exports = {

  authenticateUser: function (req, isValid) {

    req.validate({
      'device_id': 'string',
      'device_name': 'string',
      'access_token': 'string'
    });

    Auth.findOne({
      device_id: req.param('device_id'),
      device_name: req.param('device_name'),
      access_token: req.param('access_token'),
      user_agent: req.headers['user-agent']
    }).exec(function (err, auth) {

      console.log(err, !(!auth));

      if (err) {
        isValid(false);
        return;
      }

      isValid(!(!auth));

    });

  },

  generateToken: function (token, deviceId, userId, deviceName, appId, userAgent) {

    Auth.create({
      'access_token': token,
      'device_id': deviceId,
      'user_id': userId,
      'expires_in': 1,
      'status': 1,
      'created_at': Date.now(),
      'device_name': deviceName,
      'app_id': appId,
      'user_agent': userAgent

    }).exec(function (err, contact) {
      console.log(err);
    });
  }

};
