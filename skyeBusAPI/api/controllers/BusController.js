/**
 * BusController
 *
 * @description :: Server-side logic for managing buses
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {



  /**
   * `BusController.create()`
   */
  create: function (req, res) {
    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      req.validate({
        'plate_number': 'string',
        'make': 'string',
        'model': 'string',
        'status': 'string'
      });

      var bus = {
        plate_number: req.param("plate_number"),
        make: req.param("make"),
        model: req.param("model"),
        status: req.param("status")
      };

      Bus.create(bus).exec(function (err, response) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            error: err
          });
        } else {
          // New Bus created
          res.status(200).send({
            status: 1,
            message: 'New Bus Added Succesfully',
            data: response
          });
        }
      });

    });
  },


  /**
   * `BusController.update()`
   */
  update: function (req, res) {

    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      req.validate({
        'plate_number': 'string',
        'make': 'string',
        'model': 'string',
        'status': 'string'
      });

      var bus = {
        plate_number: req.param("plate_number"),
        make: req.param("make"),
        model: req.param("model"),
        status: req.param("status")
      };

      Bus.update(bus).exec(function (err, response) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            error: err
          });
        } else {
          // Bus details updated
          res.status(200).send({
            status: 1,
            message: 'Bus Details Updated Succesfully',
            data: response
          });
        }
      });

    });
  },


  /**
   * `BusController.get()`
   */
  get: function (req, res) {
    req.validate({
      id: 'string'
    });

    Bus.findOne(req.param('id')).populate('trips', {
      sort: 'content_type DESC'
    }).exec(function (err, bus) {

      if (err) return res.negotiate(err);

      return res.send({
        status: 1,
        message: "Bus fetched successfully",
        data: bus
      });
    });
  },


  /**
   * `BusController.destroy()`
   */
  destroy: function (req, res) {
    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }


      req.validate({
        'id': 'string'
      });

      var id = req.param('id');

      Bus.destroy({id: id}).exec(function (err) {
        if (err) {
          res.status(err.status).send({
            status: 'error',
            message: err.summary,
            error: err
          });
        } else {
          res.status(200).send({
            status: 1,
            message: "Bus Deleted Successfully",
            deleted: true
          });
        }
      });

    });
  }
};

