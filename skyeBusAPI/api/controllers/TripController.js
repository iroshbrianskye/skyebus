/**
 * TripController
 *
 * @description :: Server-side logic for managing Trips
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  create: function (req, res) {

    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      //PUT CODE HERE

      req.validate({
        'name': 'string',
        'tripStartTime': 'string',
        'tripEndTime': 'string',
        'tripCapacity': 'string',
        'tripType': 'string',
        'tripDescription': 'string',
        'bus_id': 'string',
        'driver_id': 'string',
        'conductor_id': 'string',
        'schoolAdmin': 'string'
      });

      var trip = {
        name: req.param("name"),
        tripStartTime: req.param("tripStartTime"),
        tripEndTime: req.param("tripEndTime"),
        tripCapacity: req.param("tripCapacity"),
        tripType: req.param("tripType"),
        tripDescription: req.param("tripDescription"),
        bus: req.param("bus_id"),
        driver: req.param("driver_id"),
        conductor: req.param("conductor_id"),
        schoolAdmin: req.param("admin_id")
      };

      Trip.create(trip).exec(function (err, response) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            error: err
          });
        } else {
          // Trip created
          res.status(200).send({
            status: 1,
            message: 'Trip Created Succesfully',
            data: response
          });
        }
      });

    });

  },

  update: function (req, res) {

    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      //PUT CODE HERE
      req.validate({
        'id': 'string',
        'name': 'string',
        'tripEndTime': 'string',
        'completed': 'string'
      });

      var id = req.param("id");
      var name = req.param("name");
      var tripEndTime = req.param("tripEndTime");
      var completed = req.param("completed");

      Trip.update({id: id}, {name: name, tripEndTime: tripEndTime, completed: completed}).exec(function (err, updated) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            data: err
          });
        } else if (updated === undefined) {
          // Trip not found
          res.status(404).send({
            status: 0,
            message: 'Trip not found'
          });
        } else {
          res.status(200).send({
            status: 1,
            message: "Trip Updated Succesfully",
            data: updated
          });
        }
      });
    });

  },

  get: function (req, res) {

    req.validate({
      'id': 'string'
    });

    var id = req.param('id');

    Trip.findOne(id).populate('tripLocationDetails', {
      sort: 'content_type DESC'
    }).exec(function (err, trip) {

      if (err) return res.negotiate(err);

      return res.send({
        status: 1,
        message: "Trip Found Successfully",
        data: trip
      });

    });
  },

  getTripsMadeByBus: function (req, res) {

    req.validate({
      'bus_id': 'string'
    });

    var id = req.param('bus_id');

    Trip.find({bus: id}).exec(function (err, trips) {
      if (err) {
        res.status(err.status).send({
          status: 0,
          message: err.summary,
          data: err
        });
      } else {
        res.status(200).send({
          status: 1,
          message: "Total Trips Made By Specific Bus Fetched Successfully",
          data: trips
        });
      }
    });
  },

  getTripsMadeByDriver: function (req, res) {

    req.validate({
      'driver_id': 'string',
    });

    var id = req.param('driver_id');

    Trip.find({driver: id, completed: true}).exec(function (err, trips) {
      if (err) {
        res.status(err.status).send({
          status: 0,
          message: err.summary,
          data: err
        });
      } else {
        res.status(200).send({
          status: 1,
          message: "Trips Completed By Specific Driver Fetched Successfully",
          data: trips
        });
      }
    });
  },

  getTripsMadeByConductor: function (req, res) {

    req.validate({
      'conductor_id': 'string',
    });

    var id = req.param('conductor_id');

    Trip.find({conductor: id, completed: true}).exec(function (err, trips) {
      if (err) {
        res.status(err.status).send({
          status: 0,
          message: err.summary,
          data: err
        });
      } else {
        res.status(200).send({
          status: 1,
          message: "Trips Completed By Specific Conductor Fetched Successfully",
          data: trips
        });
      }
    });
  },

  getTripsCreatedByAdmin: function (req, res) {

    req.validate({
      'admin_id': 'string'
    });

    var id = req.param('admin_id');

    Trip.find({schoolAdmin: id}).exec(function (err, trips) {
      if (err) {
        res.status(err.status).send({
          status: 0,
          message: err.summary,
          data: err
        });
      } else {
        res.status(200).send({
          status: 1,
          message: "Trips Created By Specific Admin Fetched Successfully",
          data: trips
        });
      }
    });
  },

  delete: function (req, res) {

    req.validate({
      'id': 'string'
    });

    var id = req.param('id');

    Trip.destroy({id: id}).exec(function (err) {
      if (err) {
        res.status(err.status).send({
          status: 'error',
          message: err.summary,
          error: err
        });
      } else {
        res.status(200).send({
          status: 1
        });
      }
    });
  }
};


