/**
 * DriverController
 *
 * @description :: Server-side logic for managing drivers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {



  /**
   * `DriverController.create()`
   */
  create: function (req, res) {

    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      //PUT CODE HERE

      req.validate({
        'first_name': 'string',
        'second_name': 'string',
        'third_name': 'string',
        'phone_number': 'string',
        'address': 'string',
        'location': 'string',
        'id_number': 'string'
      });

      var driver = {
        first_name: req.param("first_name"),
        second_name: req.param("second_name"),
        third_name: req.param("third_name"),
        phone_number: req.param("phone_number"),
        address: req.param("address"),
        location: req.param("location"),
        id_number: req.param("id_number")
      };

      Driver.create(driver).exec(function (err, response) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            error: err
          });
        } else {
          // Driver account created
          res.status(200).send({
            status: 1,
            message: 'Driver Account Created Succesfully',
            data: response
          });
        }
      });

    });

  },

  signin: function (req, res) {

    req.validate({
      'token': 'string',
      'device_name': 'string',
      'device_id': 'string',
      'app_id': 'string'
    });

    var token = req.param('token');
    var deviceName = req.param('device_name');
    var deviceId = req.param('device_id');
    var appId = req.param('app_id');
    var userAgent = req.headers['user-agent'];

    console.log(deviceName);

    var auth = new GoogleAuth;
    var client = new auth.OAuth2('', '', '');

    client.verifyIdToken(
      token,
      '',
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3],
      function (e, login) {
        console.log(e);

        var payload = login.getPayload();
        var userid = payload['sub'];
        // If request specified a G Suite domain:
        //var domain = payload['hd'];

        var userEmail = payload['email'];

        var driverToFind = {
          email: userEmail
        };

        Driver.find(driverToFind).exec(function (err, y) {

          if (err) {
            res.status(err.status).send({
              status: 0,
              message: err.summary,
              error: err
            });
            return;
          }

          //add ID to request

          AuthService.generateToken(token, deviceId, y.id, deviceName, appId, userAgent);

          return res.send({
            status: 1,
            message: 'login successful',
            driver: y,
            token: token
          });


        });
      }
    );
  },

  get: function (req, res) {
    req.validate({
      id: 'string'
    });

    Driver.findOne(req.param('id')).exec(function (err, driver) {

      if (err) return res.negotiate(err);

      return res.send({
        status: 1,
        message: "Driver account fetched successfully",
        data: driver
      });
    });

  },

  /**
   * `DriverController.update()`
   */
  update: function (req, res) {

  },


  /**
   * `DriverController.destroy()`
   */
  destroy: function (req, res) {
    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      //PUT CODE HERE

      req.validate({
        'id': 'string'
      });

      var id = req.param('id');

      Driver.destroy({id: id}).exec(function (err) {
        if (err) {
          res.status(err.status).send({
            status: 'error',
            message: err.summary,
            error: err
          });
        } else {
          res.status(200).send({
            status: 1,
            message: "Driver Account Deleted Successfully",
            deleted: true
          });
        }
      });

    });
  }
};

