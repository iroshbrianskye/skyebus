/**
 * StudentController
 *
 * @description :: Server-side logic for managing students
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {



  /**
   * `StudentController.create()`
   */
  create: function (req, res) {
    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }


      req.validate({
        'first_name': 'string',
        'second_name': 'string',
        'third_name': 'string',
        'admission_number': 'string',
        'class': 'string',
        'address': 'string',
        'location': 'string'

      });

      var student = {
        first_name: req.param("first_name"),
        second_name: req.param("second_name"),
        third_name: req.param("third_name"),
        admission_number: req.param("phone_number"),
        class: req.param("class"),
        address: req.param("address"),
        location: req.param("location")

      };

      Student.create(student).exec(function (err, response) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            error: err
          });
        } else {
          // Student account created
          res.status(200).send({
            status: 1,
            message: 'Student Account Created Succesfully',
            data: response
          });
        }
      });

    });
  },


  /**
   * `StudentController.update()`
   */
  update: function (req, res) {

  },


  /**
   * `StudentController.get()`
   */
  get: function (req, res) {
    req.validate({
      'id': 'string'
    });

    var id = req.param('id');

    Student.findOne(id).populate('parents', {
      sort: 'content_type DESC'
    }).exec(function (err, student) {

      if (err) return res.negotiate(err);

      return res.send({
        status: 1,
        message: "Student Account Found Successfully",
        data: student
      });

    });
  },

  /**
   * Get Students Scheduled on A Certain Trip
  */

  // getStudentsBookedOnTrip: function (req, res) {
  //
  //   req.validate({
  //     'trip_id': 'string'
  //   });
  //
  //   var id = req.param('trip_id');
  //
  //   Student.find({trip: id}).exec(function (err, students) {
  //     if (err) {
  //       res.status(err.status).send({
  //         status: 0,
  //         message: err.summary,
  //         data: err
  //       });
  //     } else {
  //       res.status(200).send({
  //         status: 1,
  //         message: "Success",
  //         data: students
  //       });
  //     }
  //   });
  // },


  /**
   * Get Students in Transit on a Certain Bus
   */

  // getStudentsOnBoard: function (req, res) {
  //
  //   req.validate({
  //     'bus_id': 'string'
  //   });
  //
  //   var id = req.param('bus_id');
  //
  //   Student.find({bus: id}).exec(function (err, students) {
  //     if (err) {
  //       res.status(err.status).send({
  //         status: 0,
  //         message: err.summary,
  //         data: err
  //       });
  //     } else {
  //       res.status(200).send({
  //         status: 1,
  //         message: "Success",
  //         data: students
  //       });
  //     }
  //   });
  // },


  /**
   * `StudentController.destroy()`
   */
  destroy: function (req, res) {
    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      req.validate({
        'id': 'string'
      });

      var id = req.param('id');

      Student.destroy({id: id}).exec(function (err) {
        if (err) {
          res.status(err.status).send({
            status: 'error',
            message: err.summary,
            error: err
          });
        } else {
          res.status(200).send({
            status: 1,
            message: "Student Account Deleted Successfully",
            deleted: true
          });
        }
      });

    });
  }
};

