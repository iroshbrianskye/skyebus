/**
 * BoardingsController
 *
 * @description :: Server-side logic for managing boardings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {



  /**
   * `BoardingsController.create()`
   */
  create: function (req, res) {
    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      req.validate({
        'boarding_start_time': 'string',
        'boarding_end_time': 'string',
        'boarding_status': 'string',
        'student_id': 'string',
        'trip_id': 'string'
      });

      var boarding = {
        boarding_start_time: req.param("boarding_start_time"),
        boarding_end_time: req.param("boarding_end_time"),
        boarding_status: req.param("boarding_status"),
        student_id: req.param('student_id'),
        trip_id: req.param('trip_id')
      };

      Boarding.create(boarding).exec(function (err, response) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            error: err
          });
        } else {
          // Boarding created
          res.status(200).send({
            status: 1,
            message: 'Boarding Created Succesfully',
            data: response
          });
        }
      });

    });

  },


  /**
   * `BoardingsController.update()`
   */
  update: function (req, res) {
    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      req.validate({
        'boarding_start_time': 'string',
        'boarding_end_time': 'string',
        'boarding_status': 'string',
        'student_id': 'string',
        'trip_id': 'string'
      });

      var boarding_start_time = req.param("boarding_start_time");
      var boarding_end_time = req.param("boarding_end_time");
      var boarding_status = req.param("boarding_status");
      var student_id = req.param("student_id");
      var trip_id = req.param("trip_id");


      Boarding.update({id: id}, {
        boarding_start_time: boarding_start_time,
        boarding_end_time: boarding_end_time,
        boarding_status: boarding_status,
        student_id: student_id,
        trip_id: trip_id
      }).exec(function (err, updated) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            data: err
          });
        } else if (updated === undefined) {
          // Boarding not found
          res.status(404).send({
            status: 0,
            message: 'Boarding not found'
          });
        } else {
          res.status(200).send({
            status: 1,
            message: "Boarding Updated Succesfully",
            data: updated
          });
        }
      });
    });
  },


  /**
   * `BoardingsController.get()`
   */
  get: function (req, res) {
    req.validate({
      id: 'string'
    });

    Boarding.findOne(req.param('id')).exec(function (err, boarding) {

      if (err) return res.negotiate(err);

      return res.send({
        status: 1,
        message: "Boarding fetched successfully",
        data: boarding
      });
    });
  },


  /**
   * `BoardingsController.destroy()`
   */
  destroy: function (req, res) {
    req.validate({
      'id': 'string'
    });

    var id = req.param('id');

    Boarding.destroy({id: id}).exec(function (err) {
      if (err) {
        res.status(err.status).send({
          status: 'error',
          message: err.summary,
          error: err
        });
      } else {
        res.status(200).send({
          status: 1
        });
      }
    });
  }
};

