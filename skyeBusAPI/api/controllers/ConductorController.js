/**
 * ConductorController
 *
 * @description :: Server-side logic for managing Conductors
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {



  /**
   * `ConductorController.create()`
   */
  create: function (req, res) {

    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      //PUT CODE HERE

      req.validate({
        'first_name': 'string',
        'second_name': 'string',
        'third_name': 'string',
        'phone_number': 'string',
        'address': 'string',
        'location': 'string',
        'id_number': 'string'
      });

      var conductor = {
        first_name: req.param("first_name"),
        second_name: req.param("second_name"),
        third_name: req.param("third_name"),
        phone_number: req.param("phone_number"),
        address: req.param("address"),
        location: req.param("location"),
        id_number: req.param("id_number")
      };

      Conductor.create(conductor).exec(function (err, response) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            error: err
          });
        } else {
          // Conductor account created
          res.status(200).send({
            status: 1,
            message: 'Conductor Account Created Succesfully',
            data: response
          });
        }
      });

    });

  },

  get: function (req, res) {
    req.validate({
      id: 'string'
    });

    Conductor.findOne(req.param('id')).exec(function (err, conductor) {

      if (err) return res.negotiate(err);

      return res.send({
        status: 1,
        message: "Conductor account fetched successfully",
        data: conductor
      });
    });

  },

  /**
   * `ConductorController.update()`
   */
  update: function (req, res) {

  },


  /**
   * `ConductorController.destroy()`
   */
  destroy: function (req, res) {
    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      //PUT CODE HERE

      req.validate({
        'id': 'string'
      });

      var id = req.param('id');

      Conductor.destroy({id: id}).exec(function (err) {
        if (err) {
          res.status(err.status).send({
            status: 'error',
            message: err.summary,
            error: err
          });
        } else {
          res.status(200).send({
            status: 1,
            message: "Conductor Account Deleted Successfully",
            deleted: true
          });
        }
      });

    });
  }
};


