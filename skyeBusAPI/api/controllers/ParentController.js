/**
 * ParentController
 *
 * @description :: Server-side logic for managing parents
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {



  /**
   * `ParentController.create()`
   */
  create: function (req, res) {

    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }


      req.validate({
        'first_name': 'string',
        'second_name': 'string',
        'third_name': 'string',
        'phone_number': 'string',
        'address': 'string',
        'location': 'string'

      });

      var parent = {
        first_name: req.param("first_name"),
        second_name: req.param("second_name"),
        third_name: req.param("third_name"),
        phone_number: req.param("phone_number"),
        address: req.param("address"),
        location: req.param("location")

      };

      Parent.create(parent).exec(function (err, response) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            error: err
          });
        } else {
          // Parent account created
          res.status(200).send({
            status: 1,
            message: 'Parent Account Created Succesfully',
            data: response
          });
        }
      });

    });
  },


  signin: function (req, res) {

    req.validate({
      'token': 'string',
      'device_name': 'string',
      'device_id': 'string',
      'app_id': 'string'
    });

    var token = req.param('token');
    var deviceName = req.param('device_name');
    var deviceId = req.param('device_id');
    var appId = req.param('app_id');
    var userAgent = req.headers['user-agent'];

    console.log(deviceName);

    var auth = new GoogleAuth;
    var client = new auth.OAuth2('', '', '');

    client.verifyIdToken(
      token,
      '',
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3],
      function (e, login) {
        console.log(e);

        var payload = login.getPayload();
        var userid = payload['sub'];
        // If request specified a G Suite domain:
        //var domain = payload['hd'];

        var userEmail = payload['email'];

        var parentToFind = {
          email: userEmail
        };

        Parent.find(parentToFind).exec(function (err, x) {

          if (err) {
            res.status(err.status).send({
              status: 0,
              message: err.summary,
              error: err
            });
            return;
          }

          //add ID to request

          AuthService.generateToken(token, deviceId, x.id, deviceName, appId, userAgent);

          return res.send({
            status: 1,
            message: 'login successful',
            parent: x,
            token: token
          });


        });
      }
    );

  },


  /**
   * `ParentController.update()`
   */
  update: function (req, res) {
    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }


      req.validate({
        'first_name': 'string',
        'second_name': 'string',
        'third_name': 'string',
        'phone_number': 'string',
        'address': 'string',
        'location': 'string'

      });

      var parent = {
        first_name: req.param("first_name"),
        second_name: req.param("second_name"),
        third_name: req.param("third_name"),
        phone_number: req.param("phone_number"),
        address: req.param("address"),
        location: req.param("location")

      };

      Parent.update(parent).exec(function (err, response) {
        if (err) {
          res.status(err.status).send({
            status: 0,
            message: err.summary,
            error: err
          });
        } else {
          // Parent account created
          res.status(200).send({
            status: 1,
            message: 'Parent Account Updated Succesfully',
            data: response
          });
        }
      });

    });
  },


  /**
   * `ParentController.get()`
   */
  get: function (req, res) {
    req.validate({
      'id': 'string'
    });

    var id = req.param('id');

    Parent.findOne(id).populate('students', {
      sort: 'content_type DESC'
    }).exec(function (err, parent) {

      if (err) return res.negotiate(err);

      return res.send({
        status: 1,
        message: "Parent Account Found Successfully",
        data: parent
      });

    });
  },


  /**
   * `ParentController.destroy()`
   */
  destroy: function (req, res) {
    AuthService.authenticateUser(req, function (isValid) {

      if (!isValid) {
        return res.send({
          status: -1,
          message: "Please log in to continue"
        });
      }

      req.validate({
        'id': 'string'
      });

      var id = req.param('id');

      Parent.destroy({id: id}).exec(function (err) {
        if (err) {
          res.status(err.status).send({
            status: 'error',
            message: err.summary,
            error: err
          });
        } else {
          res.status(200).send({
            status: 1,
            message: "Parent Account Deleted Successfully",
            deleted: true
          });
        }
      });

    });
  }
};

