/**
 * Trip.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: false,
      defaultsTo: ""
    },

    tripStartTime: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    tripEndTime: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    tripCapacity: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    tripType: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    tripDescription: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    completed: {
      type: 'string',
      defaultsTo: false
    },

    //Has
    tripLocationDetails: {
      collection: 'tripLocationDetails',
      via: 'trip'
    },

    //Has
    boardings: {
      collection: 'boardings',
      via: 'trip'
    },

    //BelongsTo
    bus: {
      model: 'bus'
    },

    //BelongsTo
    driver: {
      model: 'driver'
    },

    //BelongsTo
    conductor: {
      model: 'conductor'
    },

    //CreatedBy
    admin: {
      model: 'admin'
    }
  }
};

