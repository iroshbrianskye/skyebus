/**
 * Student.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    first_name: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    second_name: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    third_name: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    admission_number: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    class: {
      type: 'string',
      required: false,
      defaultsTo: ""
    },

    address: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    location: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    parents: {
      collection: 'parent',
      via: 'kids'
    },

    //Has
    boardings: {
      collection: 'boardings',
      via: 'trip'
    }
  }
};

