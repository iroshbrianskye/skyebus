/**
 * Bus.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    plateNumber: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    make: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    model: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    status: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    // Trips
    trips: {
      collection: 'trip',
      via: 'bus'
    }
  }
};

