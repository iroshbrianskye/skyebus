/**
 * TripLocationDetails.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    lat: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    lng: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    trip: {
      model: 'trip'
    }
  }
};

