/**
 * Auth.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    access_token: {
      type: 'longtext',
      required: true,
      defaultsTo: ""
    },

    user_id: {
      type: 'string',
      required: true
    },

    created_at: {
      type: 'string',
      required: true
    },

    expires_in: {
      type: 'string',
      required: true
    },

    status: {
      type: 'string',
      required: true
    },

    device_id: {
      type: 'string',
      required: true
    },

    device_name: {
      type: 'string',
      required: true
    },

    user_agent: {
      type: 'string',
      required: true
    },

    app_id: {
      type: 'string',
      required: true
    }
  }
};

