/**
 * Boardings.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    boarding_start_time: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    boarding_end_time: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    boarding_status: {
      type: 'string',
      required: true,
      defaultsTo: ""
    },

    student: {
      model: 'student'
    },

    trip: {
      model: 'trip'
    }
  }
};

